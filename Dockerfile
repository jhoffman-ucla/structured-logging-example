FROM gcc:bullseye AS logging-builder

# Build our logging library
RUN apt-get update
RUN apt-get install -y g++ cmake
ADD structlog-cpp/ /structlog-cpp
#RUN ls .. && exit 1
WORKDIR structlog-cpp/build
RUN cmake .. && make -j

# Build our simple C++ executable "job"
FROM gcc:bullseye AS exe-builder
RUN mkdir /src
COPY ./ src/
WORKDIR /src/
COPY --from=logging-builder /structlog-cpp ./structlog-cpp

RUN g++ --std=c++11 -I/src/structlog-cpp/include/ main.cpp -c main.o 
RUN g++ --std=c++11 main.o structlog-cpp/build/libspdlog.a -o logging-job

# Copy the previously built exe into our run container
FROM debian:bookworm AS runner
RUN apt-get update && apt-get install python3 python3-structlog -y

COPY --from=exe-builder src/logging-job /
COPY --from=exe-builder src/main.py /

ENTRYPOINT ["/bin/bash", "-c", "python3 main.py | tee /var/log/logging-example/example.log"]