#include <iostream>
#include <string>
#include "structlog-cpp/include/spdlog/spdlog.h"
#include "structlog-cpp/include/spdlog/json_formatter.h"
#include "structlog-cpp/include/spdlog/default_formatter.h"
#include <cstdlib>
#include <cmath>
#include <ctime>

int main(int argc, char ** argv) {

	srand(time(0));
	
	std::string uid = "undefined";
	if (argc > 1) {
		uid = std::string(argv[1]);
	}
	
	spdlog::info("run id: {}", uid)({
				{"@l", "info"},
				{"@m", "started c++ subproccess run uid " + uid},
				{"spdlog major_version", SPDLOG_VER_MAJOR},
				{"spdlog minor_version", SPDLOG_VER_MINOR},
			});


	if (round( ((double)rand()) / ((double)RAND_MAX) ) < 0.05 ) {
		spdlog::error("here's what a warning might look like") ({{"@l", "warning"}, {"@m", "here's what a warning might look like" }});
	}
	

	if (round( ((double)rand()) / ((double)RAND_MAX) ) < 0.02 ) {
		spdlog::error("something (fake) went wrong!") ({{"@l", "error"}, {"@m", "something (fake) went wrong in uid " + uid }});
	}

	return round( ((double)rand()) / ((double)RAND_MAX) );
}