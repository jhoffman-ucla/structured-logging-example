# structured-logging-example

## What (and why) is structured logging?

Structured logging allows us to ingest logs from many distributed processes/services into a single point, and then further sort, analyze, and filter them to gain insight into the operation of the larger system.  Due to the large number of processes and interoperating components, SimpleMind seems like a good fit for such centralized, structured logging.

NOTE: At this point, although the python logger *is* sending structured logs correctly, the log aggregator uses a special syntax for pretty display of those logs that relies on the "@" symbol, which pythons *kwargs* don't play nicely with.  I'll remove this message if I can find a fix for it.

## Goals of this example

- Demonstrate structured logging from C++ (see [main.cpp](main.cpp))
- Demonstrate structured logging from Python (see [main.py](main.py))
- Ingest logs into a sample service (Seq)
- Show a [simple guided example](#further-exploration) of how one might troubleshoot a failed process

## Quick start

Make sure you have Docker Desktop installed (or `docker` and `docker-compose` installed on Linux)

Then, run:
```
docker build . -t logging-example
docker-compose up 
```

Finally, open http://localhost:8080 in your browser.

Username: admin
Password: mycatsarehungry

## Overview

![diagram of container interaction](extra/diagram.png)

In this example I've simulated a python parent process that orchestrates multiple calls to a compiled C++ process, in effect trying to simulate SimpleMind's use of "agent" subprocesses.  All different processes write structured logs into a single log file (/var/log/logging-example/example.log).

Separately, we launch a container that runs [Seq](https://datalust.co/seq), a log aggregation tool that I've never used before, but for this example was easy to set up.  The Seq container runs the web interface, but also exposes an "ingest" port that we'll use to read our unified logs.

Finally, we use a logging "shim;"  in this case, it's `seqcli`, a tool from the Seq developers that can interact with the Seq server.  `seqcli` tails our log file (continuously reads the last line of the file) and then sends new loglines to the seq server on the ingest port.

## How to run (in a little more detail)

### Prequisites

- [Docker Desktop](https://www.docker.com/products/docker-desktop/) installed (Mac/Windows)

or

- `sudo apt install docker docker-desktop` (linux, adapt the command for your distro)

### 1. Build the image

```
docker build . -t logging-example
```

Apologies in advance for the large gcc image!

Unfortunately this may take a while since it's a multistage build.

### 2. Start the stack

```
docker-compose up 
```

This (should) bring up three containers:

1. The logging-example container built in the previous step
2. What we'll use for log aggregatation, "Seq"
3. Finally, a logging "shim" that will handle pushing our logs into the aggregator

### 3. Open the Seq interface

Go to http://localhost:8080

Username: admin
Password: mycatsarehungry

## Further exploration

I've tried to simulate some random, but simple scenarios.

Each launch of the C++ agent gets a unique UID.  Both the python process and the C++ subprocess log using this UID.

As an example of how we might troubleshoot problems, find an error in the logs (red dot!  Your UID will be different.)

![example of an error log line](extra/error_example.png)

From this error we can grab to UID to try and trace the error across our other processes.  In this case, the error log line came from our C++ process, so let's try and see if we can gain any more insight by looking at the results from the python process. Copy the UID and then search for other log lines that have that UID:

![example of searching for a UID](extra/search_example.png)

We should find two results (unfortunately not formatted prettily).  By looking at the later one, logged from python when the C++ process returns, shows that we throw an "error".  Your exit_code may be 0 or 256.  Although it's a fake exit code, this would indicate that the subprocess had failed and that we may need to re-run that agent (or something).

![example of search results](extra/search_results_example.png)

## Final thoughts

UCLA folks: if you run into issues or have any questions, feel free to ping me on Slack or email me!