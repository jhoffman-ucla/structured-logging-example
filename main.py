import os
import sys
import structlog
from structlog.processors import JSONRenderer, TimeStamper
from structlog.stdlib import add_log_level
import time
import random
import uuid
import json

structlog.configure(
    processors = [
        add_log_level,
        TimeStamper(fmt="iso"),
        JSONRenderer(sort_keys=True)
    ]
)

    
logger = structlog.get_logger()

def main(argc, argv):
    
    for i in range(100):

        metadata = {
            "host": os.getenv("HOSTNAME"),
            "user": os.getenv("HOME"),
            "workingdir": os.getenv("PWD"),
        }

        tracing_uid = str(uuid.uuid4())[0:8]
        
        logger.info("launching process", uid=tracing_uid, metadata=metadata)
        ret_val = os.system("./logging-job {}".format(tracing_uid))


        test = {
            "@Message": "process completed",
            "@metadata": metadata,
            "exit_code": ret_val,
            "@i": tracing_uid,
        }
        #logger.error("process completed", test)
        logger.error("process completed", uid=tracing_uid, metadata=metadata, exit_code=ret_val, Message="process completed")
        
        time.sleep(random.random())
    
if __name__=="__main__":
    main(len(sys.argv), sys.argv)